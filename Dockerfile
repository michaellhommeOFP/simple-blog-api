FROM php:cli

RUN apt-get update \
    # Linux tools
    && apt-get install -y zip \
    # PHP Extensions (https://hub.docker.com/_/php#How_to_install_more_PHP_extensions)
    && docker-php-ext-install pdo_mysql \
    # Install Composer (https://getcomposer.org/doc/faqs/how-to-install-composer-programmatically.md)
    && EXPECTED_CHECKSUM="$(php -r 'copy("https://composer.github.io/installer.sig", "php://stdout");')" \
    && php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php -r "if (hash_file('sha384', 'composer-setup.php') === '$EXPECTED_CHECKSUM') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
    && php composer-setup.php --install-dir=/usr/local/bin --filename=composer \
    && php -r "unlink('composer-setup.php');" \
    # Install Symfony (https://symfony.com/download)
    && curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.deb.sh' | bash - \
    && apt-get install -y symfony-cli \
    # Create project directory
    && mkdir /project

# Copy and initialize project
COPY ./ /project
WORKDIR /project
RUN composer install

# Configure container startup
CMD symfony serve --no-tls