## Docker deployment

### Environment variable

- `JWT_PASSPHRASE`: a passphrase for the JWT keys, required
- `DATABASE_URL`: the URL for database connexion, required

### Volume

- `config/jwt`: directory used to store the JWT keys

### Ports

Internal server is configured to listen to port 8000

### Initial setup

Database schema creation:

```
docker-compose exec backend-api symfony console doctrine:database:create
docker-compose exec backend-api symfony console doctrine:migrations:migrate --no-interaction
```

JWT keys generation:

```
docker-compose exec backend-api symfony console lexik:jwt:generate-keypair
```
